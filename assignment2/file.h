#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/**
 * Read a file fully
 * @param path relative or absolute path to file
 * @param length pointer to int64_t where the file size is going to be stored
 * @return file content (must be freed)
 */
char *readFile(char *path, int64_t *length, char *mimeType);


#endif
