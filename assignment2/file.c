#include "file.h"

char *readFile(char *path, int64_t *length, char *mimeType) {
    // ignore first /
    int fd = open(++path, O_RDONLY);
    if (fd == -1) {
        return NULL;
    }

    // get file size
    struct stat stat;
    fstat(fd, &stat);
    *length = stat.st_size + 1;

    // find mimeType
    char *type = "text/plain";
    char *fileSuffix = strrchr(path, '.');
    if (fileSuffix != NULL) {
        ++fileSuffix;
        if (strncmp(fileSuffix, "html", strlen(fileSuffix)) == 0) {
            type = "text/html";
        }
        if (strncmp(fileSuffix, "css", strlen(fileSuffix)) == 0) {
            type = "text/css";
        }
        // add more if branches for more supported file types
    }
    strcpy(mimeType, type);

    // read content and append NULL
    char *content = malloc(*length);
    read(fd, content, *length - 1);
    content[*length] = '\0';

    // close file afterwards
    close(fd);
    return content;
}
