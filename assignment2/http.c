#include "http.h"

void freeIfNotNull(void *ptr) {
    if (ptr != NULL) {
        free(ptr);
    }
}

http_request_t *parseRequest(char *buf) {
    http_request_t *request = malloc(sizeof(http_request_t));

    for (char *header = strtok(buf, LINE_DELIMITER); header != NULL; header = strtok(NULL, LINE_DELIMITER)) {
        // parse initial header
        if (strncmp(header, "GET", strlen("GET")) == 0) {
            request->method = malloc(7);
            request->path = malloc(256);
            request->version = malloc(8); // HTTP/x.x
            if (sscanf(header, "%s %s %s", request->method, request->path, request->version) != 3) {
                freeRequest(request);
                return NULL;
            }
        }

        // parse connection header
        if (strncmp(header, "Connection", strlen("Connection")) == 0) {
            char connection[10];
            if (sscanf(header, "Connection: %s", connection) != 1) {
                freeRequest(request);
                return NULL;
            }
            request->keepAlive = strncmp(connection, KEEP_ALIVE, strlen(KEEP_ALIVE)) == 0 ? 1 : 0;
        }
    }


    return request;
}

void sendResponse(int sockFd, http_response_t *response) {
    char *outputBuffer = calloc(sizeof(char), response->length + 512); // some space to write header information
    int len = sprintf(outputBuffer,
                      "HTTP/1.1 %d %s\r\nContent-Type: %s\r\nContent-Length: %d\r\nConnection: %s\r\n\r\n%s",
                      response->statusCode, response->statusName, response->contentType, response->length,
                      response->keepAlive ? KEEP_ALIVE : CLOSE, response->message);

    outputBuffer[len + 1] = '\0';

    send(sockFd, outputBuffer, len + 1, 0);
    free(outputBuffer);
}

void freeRequest(http_request_t *request) {
    freeIfNotNull(request->method);
    freeIfNotNull(request->path);
    freeIfNotNull(request->version);

    freeIfNotNull(request);
}
