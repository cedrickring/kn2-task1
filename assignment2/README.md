# Assignment 02: A Simple HTTP Server

## Requirements
- Unix system with POSIX-style socket and file support
- gcc with c11 support
- make

## Build

```bash
make
```

## Test

```
./server_web &
SERVER_PID=$!

curl -v -o - http://localhost:8080/index.html

kill -2 $SERVER_PID
```

or test this with a browser. Output should look like this:

```
Listening on port 8080
127.0.0.1:49211 - 200 HTTP/1.1 /index.html keep-alive
127.0.0.1:49211 - 200 HTTP/1.1 /styles.css keep-alive
127.0.0.1:49211 - 200 HTTP/1.1 /index.html keep-alive
127.0.0.1:49211 - 200 HTTP/1.1 /styles.css keep-alive
127.0.0.1:49211 - 404 HTTP/1.1 / close
127.0.0.1:49212 - 200 HTTP/1.1 /index.html keep-alive
127.0.0.1:49212 - 200 HTTP/1.1 /styles.css keep-alive
```

Same port for connections that are still open.

## Cleanup

```
make clean
```