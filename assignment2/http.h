#ifndef HTTP_H
#define HTTP_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>

#define LINE_DELIMITER "\r\n"
#define KEEP_ALIVE "keep-alive"
#define CLOSE "close"


typedef struct {
    char *method;
    char *version;
    char *path;
    char keepAlive;
} http_request_t;

typedef struct {
    uint32_t statusCode;
    char *contentType;
    char *statusName;
    char *message;
    uint32_t length;
    char keepAlive;
} http_response_t;

/**
 * free allocated memory
 * @param ptr memory to be free'd
 */
void freeIfNotNull(void *ptr);

/**
 * Parse incoming http message
 * @param buf incoming message
 * @return request parameters
 */
http_request_t *parseRequest(char *buf);

/**
 * Send http response to a socket
 * @param sockFd target socket fd
 * @param response http response
 */
void sendResponse(int sockFd, http_response_t *response);

/**
 * Free a request struct
 * @param request request to be free'd
 */
void freeRequest(http_request_t* request);


#endif
