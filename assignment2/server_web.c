#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "http.h"
#include "file.h"

#define PORT 8080
#define BACKLOG 10
#define BUF_LEN 1024
#define NOT_FOUND "<html><head><title>Not found</title></head><body><h1>Not found</h1><p>The requested page cannot be found.</p></body></html>"

int main() {
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("Failed to create socket\n");
        return 1;
    }

    struct sockaddr_in addr;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);

    if (bind(fd, (const struct sockaddr *) &addr, sizeof(addr)) == -1) {
        printf("Failed to bind to port %d\n", PORT);
        return 1;
    }

    if (listen(fd, BACKLOG) == -1) {
        printf("Failed to listen on port %d\n", PORT);
        return -1;
    }
    printf("Listening on port %d\n", PORT);

    int client;
    struct sockaddr_in clientAddr;
    socklen_t clientAddrLen = sizeof(clientAddr);

    // wait for incoming connections until we receive a sigint
    while (1) {
        client = accept(fd, (struct sockaddr *) &clientAddr, &clientAddrLen);
        if (client == -1) {
            printf("Couldn't accept client\n");
            return -1;
        }

        char buf[BUF_LEN];
        int len;

        receive:
        len = recv(client, buf, BUF_LEN, 0);
        if (len == -1) {
            printf("Failed to read from client\n");
            return 1;
        }

        http_request_t *request = parseRequest(buf);
        // if something isn't sent / parsed correctly
        if (request == NULL) {
            free(request);
            close(client);
            continue;
        }

        char mimeType[50];
        int64_t contentLength;
        char *content = readFile(request->path, &contentLength, mimeType);

        http_response_t response;
        if (content == NULL) {
            response.message = NOT_FOUND;
            response.length = strlen(NOT_FOUND);
            response.contentType = "text/html";
            response.statusCode = 404;
            response.statusName = "Not Found";
            response.keepAlive = 0;
        } else {
            response.length = contentLength;
            response.message = content;
            response.contentType = mimeType;
            response.statusCode = 200;
            response.statusName = "OK";
            response.keepAlive = request->keepAlive;
        }

        printf("%s:%d - %d %s %s %s\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port), response.statusCode,
               request->version, request->path, response.keepAlive ? KEEP_ALIVE : CLOSE);
        sendResponse(client, &response);
        freeIfNotNull(content);
        freeRequest(request);

        if (!response.keepAlive) {
            // close socket if keepAlive is false
            close(client);
        } else {
            // otherwise try to receive next message
            goto receive;
        }
    }

    return 0;
}