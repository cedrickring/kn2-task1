#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define BUF_LEN 1024

int connectToAddress(const char *address, const char *port) {
    struct addrinfo hints;
    struct addrinfo *ai0;
    int err;

    memset(&hints, 0, sizeof(hints));

    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if ((err = getaddrinfo(address, port, &hints, &ai0)) != 0) {
        printf("Unable to lookup IP address: %s\n", gai_strerror(err));
        return -1;
    }

    struct addrinfo *ai;
    for (ai = ai0; ai != NULL; ai = ai->ai_next) {
        // try to connect to found ip
        int fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
        if (fd == -1) {
            continue;
        }
        if (connect(fd, ai->ai_addr, ai->ai_addrlen) == -1) {
            continue;
        }
        // return connected socket if we found an ip
        return fd;
    }
    return -1;
}

int main(int argc, char **argv) {
    if (argc < 4) {
        printf("Usage: client <host> <port> <message> [message...]");
        return 0;
    }

    char *host = *++argv;
    char *port = *++argv;
    int fd = connectToAddress(host, port);
    if (fd == -1) {
        printf("Failed to connect to %s:%s\n", host, port);
        return 1;
    }


    // concat all arguments (args 3+)
    char message[BUF_LEN];
    memset(message, 0, BUF_LEN);
    while (*++argv != NULL) {
        strcat(message, *argv);
        strcat(message, " ");
    }

    if (send(fd, message, strlen(message) + 1, 0) == -1) {
        printf("Failed to send message\n");
        return 1;
    }

    int len = recv(fd, message, BUF_LEN, 0);
    if (len == -1) {
        printf("Failed to read from server\n");
        return 1;
    }

    printf("Received message: %s\n", message);

    return 0;
}