#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#define PORT 45127
#define BACKLOG 10
#define BUF_LEN 1024

int fd;

void handleSIGINT(int sig) {
    if (sig == SIGINT) {
        printf("Stopping server...\n");
        close(fd);
    }
}

int main() {
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("Failed to create socket\n");
        return 1;
    }

    struct sockaddr_in addr;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);

    if (bind(fd, (const struct sockaddr *) &addr, sizeof(addr)) == -1) {
        printf("Failed to bind to port %d\n", PORT);
        return 1;
    }

    if (listen(fd, BACKLOG) == -1) {
        printf("Failed to listen on port %d\n", PORT);
        return -1;
    }
    printf("Listening on port %d\n", PORT);

    signal(SIGINT, handleSIGINT);

    int client;
    struct sockaddr_in clientAddr;
    socklen_t clientAddrLen = sizeof(clientAddr);

    // wait for incoming connections until we receive a sigint
    while (1) {
        client = accept(fd, (struct sockaddr *) &clientAddr, &clientAddrLen);
         if (client == -1) {
            if (errno == EBADF) { // socket was closed
                break;
            }
            printf("Couldn't accept client\n");
            return -1;
        }

        printf("Client connected: %s:%d\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));

        char buf[BUF_LEN];
        int len = recv(client, buf, BUF_LEN, 0);
        if (len == -1) {
            printf("Failed to read from client\n");
            return 1;
        }
        printf("Got message from client: %s\n", buf);

        // send dummy message back to client
        const char *message = "Hello from server!";
        send(client, message, strlen(message) + 1, 0);
        close(client);
    }

    printf("Stopped server.\n");
    return 0;
}