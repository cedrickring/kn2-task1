# Assignment 01: Networking with Sockets

## Requirements
- Unix system with POSIX-style socket and file support
- gcc with c11 support
- make

## Build

```bash
make
```

## Test

```
./server &
SERVER_PID=$!

for i in 1 2 3 4 5 6 7; do
    ./client localhost 45127 Hello from client $i
done

kill -2 $SERVER_PID
```

## Cleanup

```
make clean 
```